// Pruebas
//var semana = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes"];
/*
var semana = Array.from(document.querySelectorAll('#dias p'));
console.log(semana);
var creacionArr = semana.map(dias => dias.textContent);

console.log(creacionArr);
*/

window.name = "Nombre de la ventana";

var dato = "El nombre es " + window.name + "<br>";
var ancho = "El ancho es " + window.outerWidth + "<br>";
var alto = "El alto es " + window.outerHeight + "<br>";
var anchoInterno = "Ancho interno es " + window.innerWidth + "<br>" ;
var altoInterno = "Alto interno es " + window.innerHeight + "<br>" ;

var scrollHorizontal = "Scroll Horizontal es " + window.pageXOffset + "<br>";
var scrollVertical = "Scroll Vertical es " + window.pageYOffset + "<br>";

var distanciaIzq = "Distancia izquierda es " + window.screenX + "<br>";
var distanciaSup = "Distancia superior es " + window.screenY + "<br>";

document.write(dato, ancho, alto, anchoInterno, altoInterno, scrollHorizontal, scrollVertical, distanciaIzq, distanciaSup);

var ventana;
function funcionAbrir () {
    ventana = window.open("https://translate.google.com.pe/#view=home&op=translate&sl=auto&tl=es&text=inner", "_blank", "width=500, height=400");
}
function funcionCerrar() {
    ventana.close();
}